"""
    The universes module
    ====================

    Use it to import universes dictionaries.
    Dictionaries are broken down into firstname and lastname.
    In every dictionnary, every letter matches to a name (firstname or lastname).
    The list allows of find universe for combobox.
    The last dictionnary allows of find letter for every dictionnary.
"""

list_universes = ["Star Wars", "One Piece", "Marvel"]

dic_StarWars_firstname = {
    "A": "Darth",
    "B": "Padmé",
    "C": "Jar Jar",
    "D": "Obi-wan",
    "E": "Emperor",
    "F": "Han",
    "G": "Greedo",
    "H": "Anakin",
    "I": "Count",
    "J": "Sarlacc",
    "K": "Jabba",
    "L": "Sergeant",
    "M": "Boba",
    "N": "Wicket W",
    "O": "Wampa",
    "P": "Senator",
    "Q": "Queen",
    "R": "Padawan",
    "S": "Imperial",
    "T": "General",
    "U": "Storm",
    "V": "R2",
    "W": "Luke",
    "X": "C3",
    "Y": "Mace",
    "Z": "Princess"
}

dic_StarWars_lastname = {
    "A": "Bane",
    "B": "Sidious",
    "C": "Skywalker",
    "D": "Chewbacca",
    "E": "Fett",
    "F": "Trooper",
    "G": "PO",
    "H": "Speeder",
    "I": "Docku",
    "J": "Pyt",
    "K": "Vader",
    "L": "D2",
    "M": "Palpatine",
    "N": "Grievous",
    "O": "Binks",
    "P": "Soldier",
    "Q": "Kenobi",
    "R": "Solo",
    "S": "The Hutt",
    "T": "Maul",
    "U": "Windu",
    "V": "Queen",
    "W": "Leus",
    "X": "Monster",
    "Y": "Yoda",
    "Z": "Warrick"
}

dic_OnePiece_firstname = {
    "A": "Monkey",
    "B": "Usopp",
    "C": "Roronoa",
    "D": "Reiju",
    "E": "Eustass",
    "F": "Koby",
    "G": "Marshall",
    "H": "Sanji",
    "I": "Akainu",
    "J": "Aokiji",
    "K": "Kizaru",
    "L": "Fujitora",
    "M": "Nami",
    "N": "Nico",
    "O": "Tony",
    "P": "Franky",
    "Q": "Brook",
    "R": "Jinbei",
    "S": "Hermep",
    "T": "Basil",
    "U": "Trafalgar",
    "V": "Oden",
    "W": "Portgas",
    "X": "Gold",
    "Y": "Silver",
    "Z": "Scopper"
}

dic_OnePiece_lastname = {
    "A": "Luffy",
    "B": "Vinsmoke",
    "C": "Zoro",
    "D": "Robin",
    "E": "Sakazuki",
    "F": "Kuzan",
    "G": "Borsalino",
    "H": "Issho",
    "I": "Vegapunk",
    "J": "Gaban",
    "K": "Roger",
    "L": "Rayleigh",
    "M": "Teach",
    "N": "Kozuki",
    "O": "Ace",
    "P": "Law",
    "Q": "Kid",
    "R": "Hawkins",
    "S": "Charlotte",
    "T": "Garp",
    "U": "Lucci",
    "V": "Sabo",
    "W": "Hancock",
    "X": "Crocus",
    "Y": "Dragon",
    "Z": "Mihawk"
}

dic_Marvel_firstname = {
    "A": "Bruce",
    "B": "Tony",
    "C": "Steve",
    "D": "Natasha",
    "E": "Bucky",
    "F": "Scott",
    "G": "Peter",
    "H": "Stephen",
    "I": "Logan",
    "J": "Will",
    "K": "Wanda",
    "L": "Jarvis",
    "M": "Nick",
    "N": "Carol",
    "O": "Rocket",
    "P": "Charles",
    "Q": "Erik",
    "R": "Matt",
    "S": "Jim",
    "T": "Pepper",
    "U": "Peggy",
    "V": "Howard",
    "W": "Miles",
    "X": "Pietro",
    "Y": "Sam",
    "Z": "Wade"
}

dic_Marvel_lastname = {
    "A": "Rogers",
    "B": "Carter",
    "C": "Potts",
    "D": "Stark",
    "E": "Raccoon",
    "F": "Quill",
    "G": "Parker",
    "H": "Wilson",
    "I": "Xavier",
    "J": "Maximoff",
    "K": "Banner",
    "L": "Fury",
    "M": "Denvers",
    "N": "Barnes",
    "O": "Morales",
    "P": "Strange",
    "Q": "Lang",
    "R": "Pym",
    "S": "Murdock",
    "T": "Cage",
    "U": "Jones",
    "V": "Osborn",
    "W": "Hammer",
    "X": "Rhodes",
    "Y": "Barton",
    "Z": "Roumanoff"
}

dic_universes = {"Star Wars": {"correspondance_letter_firstname": dic_StarWars_firstname,
                               "correspondance_letter_lastname": dic_StarWars_lastname},
                 "One Piece": {"correspondance_letter_firstname": dic_OnePiece_firstname,
                               "correspondance_letter_lastname": dic_OnePiece_lastname},
                 "Marvel": {"correspondance_letter_firstname": dic_Marvel_firstname,
                            "correspondance_letter_lastname": dic_Marvel_lastname}
                 }
