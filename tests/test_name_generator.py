import unittest
from name_generator import *
from universes import dic_universes


class TestNameGeneratorFunctions(unittest.TestCase):
    def test_check_name(self):
        self.assertIsNone(check_name(""))
        self.assertIsNone(check_name("Hello"))

    def test_transformation_Firstname(self):
        self.assertEqual(transformationFirstname(
            dic_universes, "Star Wars", "A"), "Darth")
        self.assertEqual(transformationFirstname(
            dic_universes, "One Piece", "N"), "Nico")
        self.assertEqual(transformationFirstname(
            dic_universes, "Marvel", "T"), "Pepper")

    def test_transformation_Lastname(self):
        self.assertEqual(transformationLastname(
            dic_universes, "Star Wars", "A"), "Bane")
        self.assertEqual(transformationLastname(
            dic_universes, "One Piece", "N"), "Kozuki")
        self.assertEqual(transformationLastname(
            dic_universes, "Marvel", "T"), "Cage")
