"""
    The name generator module
    =========================

    This is the name generator app with Python and Tkinter.
"""
from tkinter import *
from tkinter.ttk import Combobox
from name_generator import *
from universes import dic_universes, list_universes
from tkinter.messagebox import showwarning


def check_firstname():
    """
    Check firstname entry field and print a warning if needed
    :return : if entry field contains a string
    :rtype : bool
    """
    firstname = entry_firstname.get()
    if len(firstname) == 0:
        showwarning(title="Warning", message="Please give a firstname")
        return False
    return True


def check_lastname():
    """
    Check lastname entry field and print a warning if needed
    :return : if entry field contains a string
    :rtype : bool
    """
    lastname = entry_lastname.get()
    if len(lastname) == 0:
        showwarning(title="Warning", message="Please give a lastname")
        return False
    return True


def check_universe():
    """
    Check selection of a universe in combobox and print a warning if needed
    :return : if entry field contains a universe
    :rtype : bool
    """
    universe = combobox_universe.get()
    if len(universe) == 0:
        showwarning(title="Warning", message="Please select a universe")
        return False
    return True


def callback_name_fictionnal():
    """
    Callback to transform firstname and lastname in fictionnal name
    Print a warning if needed
    """
    if check_firstname() & check_lastname() & check_universe():
        firstname = entry_firstname.get()
        lastname = entry_lastname.get()
        universe = combobox_universe.get()
        letter_firstname = firstname[0].upper()
        letter_lastname = lastname[0].upper()
        if((letter_firstname < "A") | (letter_firstname > "Z") | (letter_lastname < "A") | (letter_lastname > "Z")):
            showwarning(title="Warning", message="Please give a text string")
        else:
            fictionnal_firstname = transformationFirstname(
                dic_universes, universe, letter_firstname)
            fictionnal_lastname = transformationLastname(
                dic_universes, universe, letter_lastname)
            label_resultat.config(text="".join(
                [fictionnal_firstname, " ", fictionnal_lastname]))


if __name__ == '__main__':
    """
    Name Generator App

    Create frame and configure it
    Grid configuration
    Element configuration
    """
    frame = Tk()
    frame.title("Name Generator")
    icon = PhotoImage(file='icon.png')
    frame.iconphoto(False, icon)
    frame.minsize(250, 150)

    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)
    frame.rowconfigure(4, weight=1)
    frame.rowconfigure(5, weight=1)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)
    frame.columnconfigure(2, weight=1)

    label_universes = Label(frame, text="Choose your Universe : ")
    label_universes.grid(row=0, column=0, columnspan=3)

    combobox_universe = Combobox(
        frame, values=list_universes, state="readonly")
    combobox_universe.grid(row=1, column=0, columnspan=3)

    label_question = Label(
        frame, text="What is your Universe name ?")
    label_question.grid(row=2, column=0, columnspan=3)

    label_firstname = Label(frame, text="Firstname : ")
    label_firstname.grid(row=3, column=0)

    entry_firstname = Entry(frame)
    entry_firstname.grid(row=3, column=0, columnspan=2)

    label_lastname = Label(frame, text="Lastname : ")
    label_lastname.grid(row=3, column=1)

    entry_lastname = Entry(frame)
    entry_lastname.grid(row=3, column=1, columnspan=2)

    button_submit = Button(frame, text="Submit",
                           command=callback_name_fictionnal)
    button_submit.grid(row=4, column=0, columnspan=3)

    label_fictionnal_name = Label(frame, text="Fictionnal Name :")
    label_fictionnal_name.grid(row=5, column=0, sticky='nsew')

    label_resultat = Label(frame)
    label_resultat.grid(row=5, column=1, sticky='nsew')

frame.mainloop()
